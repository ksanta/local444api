using Local444api.Models;
using Microsoft.EntityFrameworkCore;

namespace Local444api.Persistance
{
    public class Local444DbContext : DbContext
    {
          public Local444DbContext(DbContextOptions<Local444DbContext> options)
            : base(options)
        {
        }

        public DbSet<Umember> Umembers { get; set; }
        public DbSet<Note> Notes { get; set;}
    }
}