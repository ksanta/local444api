using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Local444api.Models;
using Local444api.Persistance;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Local444api.Controllers
{
    public class UmembersController : Controller
    {
        private readonly Local444DbContext context;
        public UmembersController(Local444DbContext context)
        {
            this.context = context;

        }
        [HttpGet("/api/umembers")]
        public async Task<IEnumerable<Umember>> GetUmembersAsync()
        {
            return await context.Umembers.ToListAsync();
        }
        

        [HttpGet("api/umembers/query/{term}")]
        public IActionResult GetByTerm(string term)
        {

            var item = context.Umembers
            .Where(t =>
             t.Master.Contains(term) ||
             t.Last.Contains(term));
                         
            if (item == null)
            {
               return NotFound(); 
            }
            return new ObjectResult(item);
            }
        
        [HttpGet("api/umembers/{master}")]
        public IActionResult GetByMaster(string master)
        {

            var item = context.Umembers.FirstOrDefault(t => t.Master == master);
                         
            if (item == null)
            {
               return NotFound(); 
            }
            return new ObjectResult(item);
            }
        }
}
