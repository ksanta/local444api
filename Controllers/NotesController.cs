using System.Linq;
using Local444api.Persistance;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Local444api.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Local444api.Controllers
{
    public class NotesController : Controller
    {
        private readonly Local444DbContext context;
        public NotesController(Local444DbContext context)
        {
            this.context = context;

        }
        [HttpGet("api/umembers/notes/{master}")]
        public IActionResult GetByNotesAsync(string master)
        {

            var item = context.Notes.Where(t => t.Master == master);

            {
                return new ObjectResult(item);
            }

        }

        [HttpPost("api/umembers/notes/")]
            public IActionResult Create([FromBody] Note item, Local444DbContext context)
            {
            if (item == null)
            {
                return BadRequest();
            }

            context.Notes.Add(item);
            context.SaveChanges();

            return new ObjectResult (item);
            }

        
    }
}
