using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Local444api.Models
{
    public class Umember
    {
       [Key]
        public string Master { get; set; }
        [StringLength(35)]
        public string First { get; set; }
        [StringLength(35)]
        public string Last { get; set; }
        [StringLength(100)]
        public string Address { get; set; }
        [StringLength(35)]
        public string City { get; set; }
        [StringLength(35)]
        public string Prov { get; set; }
        [StringLength(10)]
        public string Postal { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        [StringLength(35)]
        public string Company { get; set; }
        public string CompanyStatus { get; set; }
        public string UnionStatus { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime DateAdded {get; set;}
        public string Dept { get; set; }
     

        public ICollection<Note> Notes { get; set; }

        public Umember()
        {
            Notes = new Collection<Note>();
        }


    }
}