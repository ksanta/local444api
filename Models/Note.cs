using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Local444api.Models
{
    [Table("Notes")]
    public class Note
    {
        public int Id { get; set;}
        public string Master { get; set; }
        public string User { get; set; }
        public string UserNote { get; set; }
        public DateTime Date { get; set; }
    }
}