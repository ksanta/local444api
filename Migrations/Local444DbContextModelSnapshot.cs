﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Local444api.Persistance;

namespace Local444api.Migrations
{
    [DbContext(typeof(Local444DbContext))]
    partial class Local444DbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Local444api.Models.Note", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<string>("Master");

                    b.Property<string>("User");

                    b.Property<string>("UserNote");

                    b.HasKey("Id");

                    b.HasIndex("Master");

                    b.ToTable("Notes");
                });

            modelBuilder.Entity("Local444api.Models.Umember", b =>
                {
                    b.Property<string>("Master")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasMaxLength(100);

                    b.Property<string>("Cell");

                    b.Property<string>("City")
                        .HasMaxLength(35);

                    b.Property<string>("Company")
                        .HasMaxLength(35);

                    b.Property<string>("CompanyStatus");

                    b.Property<DateTime>("DateAdded");

                    b.Property<string>("Dept");

                    b.Property<string>("First")
                        .HasMaxLength(35);

                    b.Property<string>("Last")
                        .HasMaxLength(35);

                    b.Property<string>("Phone");

                    b.Property<string>("Postal")
                        .HasMaxLength(10);

                    b.Property<string>("Prov")
                        .HasMaxLength(35);

                    b.Property<DateTime>("StatusDate");

                    b.Property<string>("UnionStatus");

                    b.HasKey("Master");

                    b.ToTable("Umembers");
                });

            modelBuilder.Entity("Local444api.Models.Note", b =>
                {
                    b.HasOne("Local444api.Models.Umember")
                        .WithMany("Notes")
                        .HasForeignKey("Master");
                });
        }
    }
}
