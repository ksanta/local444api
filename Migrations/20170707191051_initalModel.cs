﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Local444api.Migrations
{
    public partial class initalModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Umembers",
                columns: table => new
                {
                    Master = table.Column<string>(nullable: false),
                    Address = table.Column<string>(maxLength: 100, nullable: true),
                    Cell = table.Column<string>(nullable: true),
                    City = table.Column<string>(maxLength: 35, nullable: true),
                    Company = table.Column<string>(maxLength: 35, nullable: true),
                    CompanyStatus = table.Column<string>(nullable: true),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    Dept = table.Column<string>(nullable: true),
                    First = table.Column<string>(maxLength: 35, nullable: true),
                    Last = table.Column<string>(maxLength: 35, nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Postal = table.Column<string>(maxLength: 10, nullable: true),
                    Prov = table.Column<string>(maxLength: 35, nullable: true),
                    StatusDate = table.Column<DateTime>(nullable: false),
                    UnionStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Umembers", x => x.Master);
                });

            migrationBuilder.CreateTable(
                name: "Note",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    Master = table.Column<string>(nullable: true),
                    User = table.Column<string>(nullable: true),
                    UserNote = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Note", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Note_Umembers_Master",
                        column: x => x.Master,
                        principalTable: "Umembers",
                        principalColumn: "Master",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Note_Master",
                table: "Note",
                column: "Master");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Note");

            migrationBuilder.DropTable(
                name: "Umembers");
        }
    }
}
